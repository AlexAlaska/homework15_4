#include <iostream>

// Print even if "isEven" == true, or odd if false
void printEvenOdd(const int n, const bool isEven)
{
    for (int val = isEven ? 0 : 1; val <= n; val += 2)
        std::cout << val << ' ';
    std::cout << std::endl;
}

int main()
{
    const int N = 30;
   
    // Print even values
    for (int i = 0; i <= N; i += 2)
        std::cout << i << ' ';
    std::cout << std::endl;

    const bool even = true;
    printEvenOdd(N, even);
    printEvenOdd(N, !even);
}
